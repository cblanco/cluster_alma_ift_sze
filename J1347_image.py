#from numpy import random 
import os
import nifty8 as ift
import resolve as rve
import configparser
from matplotlib import pyplot as plt
import numpy as np
from matplotlib.colors import LogNorm

try:
    from mpi4py import MPI

    comm = MPI.COMM_WORLD
    master = comm.Get_rank() == 0
except ImportError:
    comm = None
    master = True

from resolve.plot.baseline_histogram import visualize_weighted_residuals 

ift.random.push_sseq_from_seed(12)
# read config file
cfg = configparser.ConfigParser()
cfg.read("/path/to/resolve_config_file.cfg") #FIXME v2
obs = rve.Observation.load("/path/to/combined.resolve.npz")
obs._weight = 10*obs._weight.astype(np.float64)
obs._vis = obs._vis.astype(np.complex128)


#deefine the diffuse lognormal model
diffuse = rve.sky_model_diffuse(cfg["sky"], obs)[0]


#compute the likelihood for the diffuse emision
likelihood_energy = rve.ImagingLikelihood(obs, diffuse, 1e-8, True, nthreads=8) #returns an operator



#initial samples
for i in range(10):
    
    random_pos=ift.from_random(diffuse.domain)
    sample=diffuse(random_pos)
    ift.single_plot(sample, name=f'/home/carmen/Escritorio/MPA/newest/diffuse_sample_{i}.png')

#minimizers
ic_sampling = ift.AbsDeltaEnergyController(
    name="Sampling (linear)", deltaE=0.0, iteration_limit=50
)
ic_newton = ift.AbsDeltaEnergyController(
    name="Newton", deltaE=0.1, convergence_level=2, iteration_limit=10
)
minimizer_newton = ift.NewtonCG(ic_newton)


n_iterations = 5
n_samples = 2

#reconstruction using just diffuse emission
def inspect_callback(sl, iglobal):
    plt.imshow(sl.average(diffuse).val.T[:,:,0,0,0], origin="lower", cmap="afmhot", norm=LogNorm())
    plt.colorbar()
    plt.savefig(f'/home/carmen/Escritorio/MPA/newest/initial_iteration_logcolor_{iglobal}.png')
    plt.close()
    plt.imshow(sl.average(diffuse).val.T[:,:,0,0,0], origin="lower", cmap="afmhot")
    plt.colorbar()
    plt.savefig(f'/home/carmen/Escritorio/MPA/newest/initial_iteration_linear_{iglobal}.png')
    plt.close()
    plt.imshow(np.sqrt(sl.sample_stat(diffuse)[1].val.T[:,:,0,0,0]), origin="lower", cmap="afmhot") #1 for the variance
    plt.savefig(f'/home/carmen/Escritorio/MPA/newest/initial_iteration_linear_sd{iglobal}.png')
    plt.close()
    rve.ubik_tools.field2fits(sl.average(diffuse), f'/home/carmen/Escritorio/MPA/newest/initial_iteration_linear_{iglobal}.fits')
  


#dirty image
dirty = rve.dirty_image(obs, "uniform", diffuse.target, True, 1e-6)

plt.imshow(dirty.val[0,0,0,:,:].T, origin='lower')
plt.colorbar()
plt.show()

rve.ubik_tools.field2fits(dirty, '/home/carmen/Escritorio/MPA/newest/dirty_resolve.fits')



samples = ift.optimize_kl(
    likelihood_energy,
    n_iterations,
    n_samples,
    minimizer_newton,
    ic_sampling,
    None,
    export_operator_outputs={"sky": diffuse},#FIXME!!!!
    output_directory="results",
    inspect_callback=inspect_callback,
    comm=comm,
    resume=False
)

sky_rct = samples.average(diffuse)

inds = np.unravel_index(np.argmax(sky_rct.val), sky_rct.shape)
#inds = (0, 0, 0, 308, 236)

#point source
sky_points, pl_ops_points = rve.sky_model_points(cfg["sky"])

#both diffuse emission and point source
sky = diffuse + sky_points

#likelihood computation
likelihood_energy = rve.ImagingLikelihood(obs, sky, 1e-8, True, nthreads=8) #returns an operator


def inspect_callback2(sl, iglobal):
    
    
    plt.imshow(sl.average(diffuse).val.T[:,:,0,0,0], origin="lower", cmap="afmhot", norm=LogNorm())
    plt.colorbar()
    plt.savefig(f'/home/carmen/Escritorio/MPA/newest/iteration_logcolor_diff{iglobal}.png')
    plt.close()
    plt.imshow(sl.average(diffuse).val.T[:,:,0,0,0], origin="lower", cmap="afmhot")
    plt.colorbar()
    plt.savefig(f'/home/carmen/Escritorio/MPA/newest/iteration_linear_diff{iglobal}.png')
    plt.close()
    plt.imshow(sl.average(sky_points).val.T[:,:,0,0,0], origin="lower", cmap="afmhot", norm=LogNorm())
    plt.colorbar()
    plt.savefig(f'/home/carmen/Escritorio/MPA/newest/iteration_logcolor_pts{iglobal}.png')
    plt.close()
    plt.imshow(sl.average(sky_points).val.T[:,:,0,0,0], origin="lower", cmap="afmhot")
    plt.colorbar()
    plt.savefig(f'/home/carmen/Escritorio/MPA/newest/iteration_linear_pts{iglobal}.png')
    plt.close()

    plt.imshow(sl.average(sky).val.T[:,:,0,0,0], origin="lower", cmap="afmhot", norm=LogNorm()) #vmin google it
    plt.colorbar()
    plt.savefig(f'/home/carmen/Escritorio/MPA/newest/iteration_logcolor_sky{iglobal}.png')
    plt.close()
    rve.ubik_tools.field2fits(sl.average(sky), f'/home/carmen/Escritorio/MPA/newest/iteration_logcolor_sky{iglobal}.fits')
    rve.ubik_tools.field2fits(sl.sample_stat(sky)[1], f'/home/carmen/Escritorio/MPA/newest/stat_logcolor_sky_error{iglobal}.fits')
    plt.imshow(sl.average(sky).val.T[:,:,0,0,0], origin="lower", cmap="afmhot")
    plt.colorbar()
    plt.savefig(f'/home/carmen/Escritorio/MPA/newest/iteration_linear_sky{iglobal}.png')
    plt.close()
        
    
    


samples2 = ift.optimize_kl(
    likelihood_energy,
    n_iterations,
    n_samples,
    minimizer_newton,
    ic_sampling,
    None,
    export_operator_outputs={"sky": diffuse},#FIXME!!!!
    output_directory="results2",
    inspect_callback=inspect_callback2,
    comm=comm,
    resume=False
)


